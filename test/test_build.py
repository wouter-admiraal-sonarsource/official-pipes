import os
import tempfile
import unittest
from unittest.mock import patch, Mock, MagicMock

import yaml

from official_build import build
from official_build.build import ProcessMetadata, ProcessResult
from official_build.validators import Failures, DockerHubAPIService, BitbucketApiService
from test.fixtures import PIPE_YML, YAML_FRAGMENT, MANIFEST_YML, MANIFEST_YML_CUSTOM

BITBUCKET_AVATAR_URL = 'https://bytebucket.org/ravatar'


def configure_mock_git(mock_git_obj: MagicMock, created_at_iso: str) -> None:
    mock_git_obj.return_value.get_pipe_published_at.return_value = created_at_iso
    mock_git_obj.return_value.get_pipe_file_added_at.return_value = created_at_iso


class BuildE2EMockTest(unittest.TestCase):
    def setUp(self):
        self.directory_name = tempfile.mkdtemp()

    @patch('official_build.build.logger.error')
    @patch('official_build.build.logger.info')
    @patch.object(DockerHubAPIService, 'get_size', Mock(return_value=int(1234232)))
    @patch.object(BitbucketApiService, 'get_pipe_metadata', Mock(return_value=yaml.safe_load(PIPE_YML)))
    @patch.object(BitbucketApiService, 'get_repository_info', Mock(return_value={'links': {'avatar': {'href': BITBUCKET_AVATAR_URL}}}))
    @patch.object(BitbucketApiService, 'get_yml_definition', Mock(return_value=YAML_FRAGMENT))
    @patch.object(BitbucketApiService, 'readme_exists', Mock(return_value=True))
    @patch.object(BitbucketApiService, 'is_version_exists', Mock(return_value=True))
    @patch.object(BitbucketApiService, 'get_latest_tag', Mock(return_value='1.0.0'))
    @patch('official_build.build.GitApiService')
    def test_extract_yml_definition_is_correct(self, mock_git, _, logger_error):

        self.manifest_path = os.path.join(self.directory_name, 'manifest.yml')
        with open(self.manifest_path, 'w', encoding='utf8') as outfile:
            outfile.write(MANIFEST_YML)

        created_on = "\"b'Tue, 09 Jun 2020 10:56:00 +0000\\n'\""
        created_at_iso = '2020-06-09T10:56:00+00:00'

        configure_mock_git(mock_git, created_at_iso)
        logger_error.assert_not_called()

        process_result: ProcessResult = build.process(self.manifest_path)
        self.assertTrue(process_result.metadata.valid)

        self.assertDictEqual({
            'name': 'Bitbucket trigger pipeline',
            'description': 'Trigger a pipeline in a Bitbucket repository.',
            'category': 'Workflow automation',
            'repositoryPath': 'atlassian/bitbucket-trigger-pipeline',
            'version': '1.0.0',
            'maintainer': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
            'logo': BITBUCKET_AVATAR_URL,
            'yml': YAML_FRAGMENT,
            'tags': ['pipelines', 'pipes', 'build', 'bitbucket'],
            'timestamp': created_on,
            'created_at': created_at_iso,
        },
            process_result.data
        )

    @patch('official_build.build.logger.error')
    @patch('official_build.build.logger.info')
    @patch.object(DockerHubAPIService, 'get_size', Mock(return_value=int(99999999999999999)))
    @patch.object(BitbucketApiService, 'get_pipe_metadata', Mock(return_value=yaml.safe_load(PIPE_YML)))
    @patch.object(BitbucketApiService, 'get_repository_info', Mock(return_value={'links': {'avatar': {'href': BITBUCKET_AVATAR_URL}}}))
    @patch.object(BitbucketApiService, 'get_yml_definition', Mock(return_value=YAML_FRAGMENT))
    @patch.object(BitbucketApiService, 'readme_exists', Mock(return_value=True))
    @patch.object(BitbucketApiService, 'is_version_exists', Mock(return_value=True))
    @patch.object(BitbucketApiService, 'get_latest_tag', Mock(return_value='1.0.0'))
    @patch('official_build.build.GitApiService')
    def test_extract_yml_definition_is_correct_too_large_docker_image(self, mock_git, _, logger_error):

        self.manifest_path = os.path.join(self.directory_name, 'manifest.yml')
        with open(self.manifest_path, 'w', encoding='utf8') as outfile:
            outfile.write(MANIFEST_YML_CUSTOM)

        created_on = "\"b'Tue, 09 Jun 2020 10:56:00 +0000\\n'\""
        created_at_iso = '2020-06-09T10:56:00+00:00'

        configure_mock_git(mock_git, created_at_iso)
        logger_error.assert_not_called()

        process_result: ProcessResult = build.process(self.manifest_path)

        expected_result = ProcessResult(
            metadata=ProcessMetadata(
                file=self.manifest_path,
                valid=False,
                failures=Failures(
                    criticals=[
                        f'{self.manifest_path} yml field not equal',
                        "Docker image "
                        "'bitbucketpipelines/pipelines-trigger-build:1.0.0' "
                        "is larger than 1GB."
                    ],
                    warnings=[
                        '1.0.0 version available. Current version is 0.1.0'
                    ],
                    skipped=[]
                ),
                errors=[]
            ),
            data={
                'name': 'Bitbucket trigger pipeline',
                'description': 'Trigger a pipeline in a Bitbucket repository.',
                'category': 'Workflow automation',
                'logo': 'my-logo.svg',
                'repositoryPath': 'atlassian/bitbucket-trigger-pipeline',
                'version': '0.1.0',
                'maintainer': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
                'yml': "- pipe: atlassian/bitbucket-trigger-pipeline:0.1.0\n  variables:\n    APP_PASSWORD: $APP_PASSWORD\n    REPO: 'your-awesome-repo'\n",
                'tags': ['pipelines', 'pipes', 'build', 'bitbucket'],
                'timestamp': created_on,
                'created_at': created_at_iso,
            }
        )
        self.assertEqual(expected_result, process_result)
